<div class="row">
	<div class="col-md-4 col-sm-6 col-xs-10">
		<?= form_open('admin/jadwal/post_edit/'.$id_jadwal); ?>
			<div class="form-group">
				<label>Nama Dokter :</label>
				<select name="id_dokter" class="form-control" required>
					<option value="" disabled>-- Pilih --</option>
					<?php if(!empty($dokter)): ?>
					<?php foreach ($dokter as $v): ?>
						<?php if($isi['nama_jadwal']==$v->id_dokter): ?>
						<option value="<?php echo $v->id_dokter; ?>" selected><?php echo $v->nama_dokter; ?></option>
						<?php else: ?>
						<option value="<?php echo $v->id_dokter; ?>"><?php echo $v->nama_dokter; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Poli :</label>
				<select name="id_poli" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<?php if(!empty($poli)): ?>
					<?php foreach($poli as $v): ?>
						<?php if($isi['id_poli']==$v->id_poli): ?>
						<option value="<?php echo $v->id_poli; ?>" selected><?php echo $v->nama_poli; ?></option>
						<?php else: ?>
						<option value="<?php echo $v->id_poli; ?>"><?php echo $v->nama_poli; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Hari :</label>
				<select name="hari" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<option value="1" <?php echo ($isi['hari']==1)?'selected':''; ?>>Senin</option>
					<option value="2" <?php echo ($isi['hari']==2)?'selected':''; ?>>Selasa</option>
					<option value="3" <?php echo ($isi['hari']==3)?'selected':''; ?>>Rabu</option>
					<option value="4" <?php echo ($isi['hari']==4)?'selected':''; ?>>Kamis</option>
					<option value="5" <?php echo ($isi['hari']==5)?'selected':''; ?>>Jumat</option>
					<option value="6" <?php echo ($isi['hari']==6)?'selected':''; ?>>Sabtu</option>
					<option value="7" <?php echo ($isi['hari']==7)?'selected':''; ?>>Minggu</option>
				</select>
			</div>
			<div class="form-group">
				<label>Jam Mulai :</label>
				<input type="time" name="jam_mulai" class="form-control" value="<?=$isi['jam_mulai'];?>">	
			</div>
			<div class="form-group">
				<label>Jam Akhir :</label>
				<input type="time" name="jam_akhir" class="form-control" value="<?=$isi['jam_akhir'];?>">	
			</div>
			<input type="submit" name="edit" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/jadwal/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>