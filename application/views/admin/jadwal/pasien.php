<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="row">
		<div class="col-md-12">
			<a href="<?php echo base_url('admin/jadwal/form_tambah_pasien'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
			<?php if (count($riwayat1)>0) { ?>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Pasien</th>
						<th>NIk/BPJS</th>
						<th>Jenis Kelamin</th>
						<th>Tanggal Berkunjung</th>
						<th>Poli</th>
						<th>No. Antrian</th>
						<th width="180">Opsi</th>
					</tr>
				</thead>
				<tbody>
			<?php
				foreach ($riwayat1 as $k => $v) {
			?>
				<tr>
					<td><?php echo ($k+1); ?></td>
					<td><?php echo $v->nama_pasien; ?></td>
					<td><?php echo $v->nik; ?></td>
					<td><?php echo $v->jenis_kelamin; ?></td>
					<td><?php echo $v->tanggal; ?></td>
					<td><?php if ($v->id_poli == '1') {
							echo "Poli Gigi";
						}elseif ($v->id_poli == '2') {
							echo "Poli THT";
						}elseif($v->id_poli == '3'){
							echo "Poli Umum";
						}elseif($v->id_poli =='5'){
							echo "Poli Bedah";
						} ?></td>
					<td><?php echo $v->no_antrian; ?></td>
					<td>
						<a href="<?php echo base_url('admin/jadwal/form_edit_pasien/'.$v->id); ?>" class="btn btn-info"><span class="fa fa-pencil"></span></a>
						<a href="<?php echo base_url('admin/jadwal/hapus_pasien/'.$v->id); ?>" class="btn btn-danger"><span class="fa fa-trash-o"></span></a>
					</td>
				</tr>
			<?php
				}
			?>
				</tbody>
			</table>
			<?php
				} else {
					echo "<h4 style='text-align:center;'><i>Data Masih Kosong!</i></h4>";
				}
			?>
		</div>
	</div>
</section>
