<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="row">
		<div class="col-md-12">
			<a href="<?php echo base_url('admin/pasien/form_tambah'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
			<?php if (count($pasien)>0) { ?>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th>No.</th>
						<th>NIK</th>
						<th>Nama pasien</th>
						<th>Tempat Lahir</th>
						<th>Tgl Lahir</th>
						<th>Jenis Kelamin</th>
						<th>Gol. Darah</th>
						<th>Agama</th>
						<th>No. HP</th>
						<!-- <th>Alamat</th> -->
						<!-- <th>Username</th> -->
						<th width="180">Opsi</th>
					</tr>
				</thead>
				<tbody>
			<?php
				foreach ($pasien as $k => $v) {
			?>
				<tr>
					<td><?php echo ($k+1); ?></td>
					<td><?php echo $v->nik; ?></td>
					<td><?php echo $v->nama_pasien; ?></td>
					<td><?php echo $v->tempat_lahir; ?></td>
					<td><?php
						$tgl_lahir='';
						$tgl_lahir_post=$v->tgl_lahir;
						if (!empty($tgl_lahir_post) AND $tgl_lahir_post!='0000-00-00') {
							$tgl_lahir = date('d-m-Y',strtotime($tgl_lahir_post));
						}
						echo $tgl_lahir;
					?></td>
					<td><?php echo $v->jenis_kelamin; ?></td>
					<td><?php echo $v->gol_darah; ?></td>
					<td><?php echo $v->agama; ?></td>
					<td><?php echo $v->no_hp; ?></td>
					<!-- <td><?php echo $v->alamat; ?></td> -->
					<!-- <td><?php echo $v->username; ?></td> -->
					<td>
						<a href="<?php echo base_url('admin/pasien/form_edit/'.$v->id_pasien); ?>" class="btn btn-info"><span class="fa fa-pencil"></span></a>
						<a href="<?php echo base_url('admin/pasien/hapus/'.$v->id_pasien); ?>" class="btn btn-danger"><span class="fa fa-trash-o"></span></a>
					</td>
				</tr>
			<?php
				}
			?>
				</tbody>
			</table>
			<?php
				} else {
					echo "<h4 style='text-align:center;'><i>Data Masih Kosong!</i></h4>";
				}
			?>
		</div>
	</div>
</section>
