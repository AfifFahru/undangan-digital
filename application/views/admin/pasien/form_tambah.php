<div class="row">
	<div class="col-md-4">
		<?= form_open('admin/pasien/post_tambah'); ?>
			<div class="form-group">
				<label>NIK :</label>
				<input type="text" name="nik" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>Nama Pasien :</label>
				<input type="text" name="nama_pasien" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>Tempat Lahir :</label>
				<input type="text" name="tempat_lahir" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>Tanggal Lahir :</label>
				<!-- <input type="date" name="tgl_lahir" class="form-control" required> -->
				<div class="input-group date" data-date="" data-date-format="yyyy-mm-dd">
					<input class="form-control input-sm" type="text" name="tgl_lahir" placeholder="Tanggal Lahir" readonly required />
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label>Jenis Kelamin :</label>
				<select name="jenis_kelamin" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<option value="L">Laki- laki</option>
					<option value="P">Perempuan</option>
				</select>
			</div>
			<div class="form-group">
				<label>Gol. Darah :</label>
				<select name="gol_darah" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<option value="A">A</option>
					<option value="B">B</option>
					<option value="O">O</option>
					<option value="AB">AB</option>
				</select>
			</div>
			<div class="form-group">
				<label>Agama :</label>
				<input type="text" name="agama" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>No. HP :</label>
				<input type="text" name="no_hp" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>Alamat :</label>
				<textarea rows="3" name="alamat" class="form-control" required></textarea>
			</div>
			<div class="form-group">
				<label>Username :</label>
				<input type="text" name="username" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>Password :</label>
				<input type="password" name="password" class="form-control">	
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/pasien/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>
<!-- <script src="<?php //echo base_url();?>template/plugins/jQuery/jquery-2.2.3.min.js"></script> -->
<link rel="stylesheet" href="<?php echo base_url().'template/plugins/datepicker/datepicker3.css'; ?>" type="text/css" />
<script type="text/javascript" src="<?php echo base_url().'template/plugins/datepicker/bootstrap-datepicker.js'; ?>"></script>
<script type="text/javascript">
$().ready(function() {
	//options method for call datepicker
	$(".input-group.date").datepicker({ 
		autoclose: true, 
		todayHighlight: true,
		viewMode: 'date',
		format: 'yyyy-mm-dd',
		changeDate: false
	});
});	
</script>