<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="row">
		<div class="col-md-12">
			<a href="<?php echo base_url('admin/antrian/form_tambah'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
			<?php if (count($antrian)>0) { ?>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th>No.</th>
						<th>NIK</th>
						<th>Tanggal Antri</th>
						<th>Jam Antri</th>
						<th>No. Antrian</th>
						<th>Poli</th>
						<th width="180">Opsi</th>
					</tr>
				</thead>
				<tbody>
			<?php
				foreach ($antrian as $k => $v) {
			?>
				<tr>
					<td><?php echo ($k+1); ?></td>
					<td><?php echo $v->nik; ?></td>
					<td><?php
						$tanggal_antri='';
						$tanggal_antri_post=$v->tanggal_antri;
						if (!empty($tanggal_antri_post) AND $tanggal_antri_post!='0000-00-00') {
							$tanggal_antri = date('d-m-Y',strtotime($tanggal_antri_post));
						}
						echo $tanggal_antri;
					?></td>
					<td><?php echo $v->jam_antri; ?></td>
					<td><?php echo $v->no_antrian; ?></td>
					<td><?php echo $v->nama_poli; ?></td>
					<td>
						<a href="<?php echo base_url('admin/antrian/form_edit/'.$v->id_antrian); ?>" class="btn btn-info"><span class="fa fa-pencil"></span></a>
						<a href="<?php echo base_url('admin/antrian/hapus/'.$v->id_antrian); ?>" class="btn btn-danger"><span class="fa fa-trash-o"></span></a>
					</td>
				</tr>
			<?php
				}
			?>
				</tbody>
			</table>
			<?php
				} else {
					echo "<h4 style='text-align:center;'><i>Data Masih Kosong!</i></h4>";
				}
			?>
		</div>
	</div>
</section>
