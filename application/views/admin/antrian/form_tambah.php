<div class="row">
	<div class="col-md-4">
		<?= form_open('admin/antrian/post_tambah'); ?>
			<div class="form-group">
				<label>NIK :</label>
				<input type="text" name="nik" class="form-control" required>
			</div>
			<div class="form-group">
				<label>No. Antrian :</label>
				<input type="number" name="no_antrian" class="form-control" required min="1" id="no-antrian">
			</div>
			<div class="form-group">
				<label>Poli :</label>
				<select name="id_poli" class="form-control" required id="pilih-poli">
					<option value="" selected disabled>-- Pilih --</option>
					<?php foreach($poli as $v): ?>
					<option value="<?php echo $v->id_poli; ?>"><?php echo $v->nama_poli; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/antrian/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>
<script src="<?=base_url();?>template/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript">
    $('#pilih-poli').change(function(){
        $.ajax({
            'method': 'GET',
            'url': '<?php echo base_url('admin/antrian/get_no_antrian');?>',
            'data': {id_poli:$(this).val()},
            'success': function(data){
                $('#no-antrian').val(data);
                // alert(data);
                // console.log('success');
            }
        });
    });
</script>