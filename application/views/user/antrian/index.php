<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<?php foreach($poli as $k=>$data_poli): ?>
	<?php
		$no=$k+1;
		if ($no%4==1) {
			$bgColor='bg-green';
		} else if ($no%4==2) {
			$bgColor='bg-aqua';
		} else if ($no%4==3) {
			$bgColor='bg-yellow';
		} else {
			$bgColor='bg-red';
		}
	?>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="small-box <?php echo $bgColor; ?>">
			<div class="inner">
				<?php $antrian=@get_antrian($data_poli->id_poli); ?>
				<h5>No. Antrian Sekarang</h5>
				<h3 id="no_antrian_sekarang_teks_<?php echo @$data_poli->id_poli; ?>"><?php echo @$data_poli->antrian_saat_ini; ?></h3>
				<p><?php echo $data_poli->nama_poli; ?></p>
			</div>
			<div class="icon">
				<i class="fa fa-briefcase"></i>
			</div>
			<div class="row" style="text-align:center;">
				<div class="col-md-12">
					<h6>No. Antrian Saya</h6>
					<?php echo @$antrian['no_antrian']; ?>
					<p>
						<?php
							// Satu antrian dilayani selama {$waktu_tunggu_menit} Menit.
							$waktu_tunggu_menit=30;
							$estimasi_waktu=0;
							$no_antrian_saya = @$antrian['no_antrian'];
							$no_antrian_sekarang = @$data_poli->antrian_saat_ini;
							if ($no_antrian_saya>0 && $no_antrian_sekarang>0) {
								if ($no_antrian_saya > $no_antrian_sekarang) {
									$estimasi_waktu = ($no_antrian_saya-$no_antrian_sekarang)*$waktu_tunggu_menit;
								}
							}
							$jam=0;$menit=0;$estimasi_teks="";
							if (!empty($no_antrian_saya) AND $no_antrian_saya!='-') {
								if ($estimasi_waktu>=60) {
									//dibulatkan ke bawah, 1,5 menjadi 1
									$jam = floor($estimasi_waktu/60);
									//estimasi_waktu/60, sisanya berapa
									$menit = $estimasi_waktu%60;
								} else {
									$menit = $estimasi_waktu;
								}
								// if ($estimasi_waktu>0) {
									$estimasi_teks.="Estimasi Waktu: ";
								// } else {
								// 	$estimasi_teks.="&nbsp;";
								// }
								if ($jam>0) {
									$estimasi_teks.=$jam." Jam ";
								}
								// if ($menit>0) {
									$estimasi_teks.=$menit." Menit";
								// }
							} else {
								$estimasi_teks.="&nbsp;";
							}
							echo $estimasi_teks;
						?>
					</p>
				</div>
				<!-- <div class="col-md-6">
					<h6>Total Antrian</h6>
					<?php //echo @$antrian['total_antrian']; ?>
				</div> -->
			</div>
			<?php //echo base_url('antrian/tambah/'.@$data_poli->id_poli) ?>
			<a href="<?php echo base_url('pasien/index/'.@$data_poli->id_poli); ?>" class="small-box-footer">
				Ambil Antrian <i class="fa fa-arrow-right"></i>
			</a>
		</div>
	</div>
	<?php if ($no%2==0): ?>
	<div class="clearfix visible-sm-block"></div>
	<?php endif; ?>
	<!-- /.col -->
	<?php endforeach; ?>
</div>
<!-- /.row -->
</section>
<!-- /.content-->
<script src="<?=base_url();?>template/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript">
	(function() {
		$(document).ready(function() {cekAntrian();});

		function cekAntrian() {
			<?php foreach($poli as $k=>$data_poli2): ?>
			$.ajax({
				'method': 'GET',
				'url': '<?php echo base_url('antrian/cek_antrian_sekarang');?>',
				'data': {id_poli: "<?php echo @$data_poli2->id_poli; ?>"},
				'success': function(data){
					$('#no_antrian_sekarang_teks_'+"<?php echo @$data_poli2->id_poli; ?>").html(data);
					// alert(data);
					// console.log('success');
				}
			});
			<?php endforeach; ?>
			setTimeout(cekAntrian, 1000);
			// 1000 miliseconds = 1 seconds = 1 detik
		}
	})();
</script>