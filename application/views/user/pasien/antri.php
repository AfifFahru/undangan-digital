
<div class="row">
	<div class="col-md-6">
		<?= form_open('pasien/post_tambah'); ?>
		<div class="form-group">
					<!-- <label>poli :</label> -->
					<input type="hidden" name="poli" class="form-control" value="<?= $isi['id_poli'];?>">	
				</div>
			<div class="form-group">
					<label>Nama Pasien :</label>
					<input type="text" name="nama" class="form-control"  readonly="true" value="<?php echo @get_pasien()['nama_pasien'];?>" required>	
			</div>
			<div class="form-group">
				<label>No. Bpjs :</label>
				<input type="text" name="no_bpjs" class="form-control" value="<?php echo @get_pasien()['nik']; ?>" readonly>	
			</div>
			<div class="form-group">
				<label>Tanggal :</label>
				<input type="date" name="tgl" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>Jam :</label>
				<input type="time" name="jam" class="form-control" required>	
			</div>
			
			<div class="form-group">
				<label>No. Antrian :</label>
				<?php $antrian=@get_antrian($id_poli); ?>
				<?php //var_dump($antrian);die(); ?>
				<input type="Number" name="nomor" class="form-control" value="<?php echo @$antrian['no_antrian'] + 1; ?>" readonly>	
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('pasien/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
	