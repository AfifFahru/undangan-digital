<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PasienController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Pasien');
		$this->load->model('Poli');
		$this->load->model('Antrian');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index($id_poli=0)
	{
		if ($this->session->userdata('username') != '') {
			$head['title'] 	= 'Beranda';
			$username 		= $this->session->userdata('username');
			$data['user'] 	= $this->Pasien->detail($username)->row_array();
			$user_id 		= @$data['user']['id'];
			$data['isi'] 	= array();
			$data['poli']	= $this->Poli->get_all()->result();
			$data['poli_pilihan'] = $id_poli;
			$data['isi'] 	 = $this->Poli->detail($id_poli)->row_array();
			$data['antrian'] = $this->Antrian->get_total_antrian($id_poli)->row_array();
			// $data['ambil']	= $this->Antrian->ambil_antrian($id_poli);
			$this->load->view('user/templates/header');
			$this->load->view('user/pasien/index',$data);
			$this->load->view('user/templates/footer');
		} else {
			$this->load->view('user/pasien/login');
		}
	}

	public function form_register(){
		$head['title'] = 'Form Register Akun';
		$data['isi'] = array();
		$this->load->view('user/templates/header');
		$this->load->view('user/pasien/register',$data);
		$this->load->view('user/templates/footer');
	}
	public function post_register(){
		$date='';
		if (!empty($this->input->post('tgl_lahir'))) {
			$date = date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		}
		$data = [
			'username'		=> $this->input->post('username'),
			'password'		=> md5($this->input->post('password')),
			'nik'			=> $this->input->post('nik'),
			'nama_pasien'	=> $this->input->post('nama_pasien'),
			'tempat_lahir'	=> $this->input->post('tempat_lahir'),
			'tgl_lahir'		=> $date,
			'jenis_kelamin'	=> $this->input->post('jenis_kelamin'),
			'gol_darah'		=> $this->input->post('gol_darah'),
			'agama'			=> $this->input->post('agama'),
			'alamat'		=> $this->input->post('alamat'),
		];
		try {
			$cek = $this->Pasien->insert($data);
			$this->session->set_flashdata('info','Pendaftaran Akun Berhasil!');
		} catch (Exception $e) {
			$this->session->set_flashdata('error','Pendaftaran Akun Gagal!');
		}
		redirect();
	}
	public function profil()
	{
		$head['title'] = 'Profil';
		$data['isi'] = array();
		// $data['pasien']	= $this->Pasien->get_pasien()->result();
		$this->load->view('user/templates/header');
		$this->load->view('user/pasien/profil',$data);
		$this->load->view('user/templates/footer');
	}
	public function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
        $cek = $this->Pasien->login($username,$password)->row_array();
		if ($cek != null) {
			$this->session->set_userdata('username',$username);
			$this->session->set_userdata('nik_login',@$cek['nik']);
			redirect('antrian/index');
		} else {
			$this->session->set_flashdata('error','Username atau Password Salah!');
			redirect();
		}	
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect();
	}
}
