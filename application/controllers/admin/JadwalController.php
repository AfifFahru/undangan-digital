<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Jadwal');
		$this->load->model('Dokter');
		$this->load->model('Pengguna');
		$this->load->model('Poli');
		date_default_timezone_set('Asia/Jakarta');

		if (empty(@$this->session->userdata('username'))) {
			redirect('admin');
		}
	}
	public function index()
	{
		$head['title'] 	= 'Data Jadwal';
		$data['jadwal']	= $this->Jadwal->get_all()->result();
		$this->load->view('admin/templates/header',$head);
		$this->load->view('admin/jadwal/index',$data);
		$this->load->view('admin/templates/footer');
	}
	public function form_tambah(){
		$head['title']  = 'Form Tambah Data Jadwal';
		$data['isi'] = array();
		$data['dokter'] = $this->Dokter->get_all()->result();
		$data['poli'] = $this->Poli->get_all()->result();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/jadwal/form_tambah',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_tambah(){
		$data = [
			'id_dokter'		=> $this->input->post('id_dokter'),
			'id_poli'		=> $this->input->post('id_poli'),
			'hari'			=> $this->input->post('hari'),
			'jam_mulai'		=> $this->input->post('jam_mulai'),
			'jam_akhir'		=> $this->input->post('jam_akhir'),
		];
		try {
			$cek = $this->Jadwal->insert($data);
			$this->session->set_flashdata('info','Data Jadwal Berhasil Ditambahkan!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Jadwal Gagal Ditambahkan!');
		}
		redirect('admin/jadwal/index');
	}
	public function form_edit($id_jadwal){
		$head['title'] 	 = 'Form Edit Data Jadwal';
		$data['isi'] 	 = $this->Jadwal->detail($id_jadwal)->row_array();
		$data['id_jadwal'] = $id_jadwal;
		$data['dokter'] = $this->Dokter->get_all()->result();
		$data['poli'] = $this->Poli->get_all()->result();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/jadwal/form_edit',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_edit($id){
		$data = [
			'id_dokter'		=> $this->input->post('id_dokter'),
			'id_poli'		=> $this->input->post('id_poli'),
			'hari'			=> $this->input->post('hari'),
			'jam_mulai'		=> $this->input->post('jam_mulai'),
			'jam_akhir'		=> $this->input->post('jam_akhir'),
		];
		try {
			$cek = $this->Jadwal->update($data, $id);
			$this->session->set_flashdata('info','Data Jadwal Berhasil Diubah!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Jadwal Gagal Diubah!');
		}
		redirect('admin/jadwal/index');
	}
	public function hapus($id){
		try {
			$cek = $this->Jadwal->delete($id);
			$this->session->set_flashdata('info', 'Data Jadwal Berhasil Dihapus!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger', 'Data Jadwal Gagal Dihapus!');
		}
		redirect('admin/jadwal/index');
	}
}