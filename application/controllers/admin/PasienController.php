<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PasienController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Pasien');
		date_default_timezone_set('Asia/Jakarta');

		if (empty(@$this->session->userdata('username'))) {
			redirect('admin');
		}
	}
	public function index()
	{
		$head['title'] 	= 'Data Pasien';
		$data['pasien']	= $this->Pasien->get_all()->result();
		$this->load->view('admin/templates/header',$head);
		$this->load->view('admin/pasien/index',$data);
		$this->load->view('admin/templates/footer');
	}
	public function form_tambah(){
		$head['title'] = 'Form Tambah Data Pasien';
		$data['isi'] = array();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/pasien/form_tambah',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_tambah(){
		$password = '12345';
		if (!empty($this->input->post('password'))) {
			$password = md5($this->input->post('password'));
		}
		$tgl_lahir='';
		$tgl_lahir_post=$this->input->post('tgl_lahir');
		if (!empty($tgl_lahir_post) AND $tgl_lahir_post!='0000-00-00') {
			$tgl_lahir = date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		}
		$data = [
			'nik'			=> $this->input->post('nik'),
			'nama_pasien'	=> $this->input->post('nama_pasien'),
			'tempat_lahir'	=> $this->input->post('tempat_lahir'),
			'tgl_lahir'		=> $tgl_lahir,
			'jenis_kelamin'	=> $this->input->post('jenis_kelamin'),
			'gol_darah'		=> $this->input->post('gol_darah'),
			'agama'			=> $this->input->post('agama'),
			'no_hp'			=> $this->input->post('no_hp'),
			'alamat'		=> $this->input->post('alamat'),
			'username'		=> $this->input->post('username'),
			'password'		=> $password,
		];
		try {
			$cek = $this->Pasien->insert($data);
			$this->session->set_flashdata('info','Data Pasien Berhasil Ditambahkan!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Pasien Gagal Ditambahkan!');
		}
		redirect('admin/pasien/index');
	}
	public function form_edit($id_pasien){
		$head['title'] 	 = 'Form Edit Data Pasien';
		$data['isi'] 	 = $this->Pasien->detail_id($id_pasien)->row_array();
		$data['id_pasien'] = $id_pasien;
		$this->load->view('admin/templates/header');
		$this->load->view('admin/pasien/form_edit',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_edit($id_pasien){
		$tgl_lahir='';
		$tgl_lahir_post=$this->input->post('tgl_lahir');
		if (!empty($tgl_lahir_post) AND $tgl_lahir_post!='0000-00-00') {
			$tgl_lahir = date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		}
		$data = [
			'nik'			=> $this->input->post('nik'),
			'nama_pasien'	=> $this->input->post('nama_pasien'),
			'tempat_lahir'	=> $this->input->post('tempat_lahir'),
			'tgl_lahir'		=> $tgl_lahir,
			'jenis_kelamin'	=> $this->input->post('jenis_kelamin'),
			'gol_darah'		=> $this->input->post('gol_darah'),
			'agama'			=> $this->input->post('agama'),
			'no_hp'			=> $this->input->post('no_hp'),
			'alamat'		=> $this->input->post('alamat'),
			'username'		=> $this->input->post('username'),
		];
		try {
			$cek = $this->Pasien->update($data, $id_pasien);
			$this->session->set_flashdata('info','Data Pasien Berhasil Diubah!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Pasien Gagal Diubah!');
		}
		redirect('admin/pasien/index');
	}
	public function hapus($id_pasien){
		try {
			$cek = $this->Pasien->delete($id_pasien);
			$this->session->set_flashdata('info', 'Data Pasien Berhasil Dihapus!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger', 'Data Pasien Gagal Dihapus!');
		}
		redirect('admin/pasien/index');
	}
}