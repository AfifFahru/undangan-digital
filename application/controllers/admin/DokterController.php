<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DokterController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Dokter');
		date_default_timezone_set('Asia/Jakarta');

		if (empty(@$this->session->userdata('username'))) {
			redirect('admin');
		}
	}
	public function index()
	{
		$head['title'] 	= 'Data Dokter';
		$data['dokter']	= $this->Dokter->get_all()->result();
		$this->load->view('admin/templates/header',$head);
		$this->load->view('admin/dokter/index',$data);
		$this->load->view('admin/templates/footer');
	}
	public function form_tambah(){
		$head['title'] = 'Form Tambah Data Dokter';
		$data['isi'] = array();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/dokter/form_tambah',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_tambah(){
		$data = [
			'nama_dokter'	=> $this->input->post('nama_dokter'),
			'no_hp'			=> $this->input->post('no_hp'),
			'alamat'		=> $this->input->post('alamat'),
		];
		try {
			$cek = $this->Dokter->insert($data);
			$this->session->set_flashdata('info','Data Dokter Berhasil Ditambahkan!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Dokter Gagal Ditambahkan!');
		}
		redirect('admin/dokter/index');
	}
	public function form_edit($id_dokter){
		$head['title'] 	 = 'Form Edit Data Dokter';
		$data['isi'] 	 = $this->Dokter->detail($id_dokter)->row_array();
		$data['id_dokter'] = $id_dokter;
		$this->load->view('admin/templates/header');
		$this->load->view('admin/dokter/form_edit',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_edit($id){
		$data = [
			'nama_dokter'	=> $this->input->post('nama_dokter'),
			'no_hp'			=> $this->input->post('no_hp'),
			'alamat'		=> $this->input->post('alamat'),
		];
		try {
			$cek = $this->Dokter->update($data, $id);
			$this->session->set_flashdata('info','Data Dokter Berhasil Diubah!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Dokter Gagal Diubah!');
		}
		redirect('admin/dokter/index');
	}
	public function hapus($id){
		try {
			$cek = $this->Dokter->delete($id);
			$this->session->set_flashdata('info', 'Data Dokter Berhasil Dihapus!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger', 'Data Dokter Gagal Dihapus!');
		}
		redirect('admin/dokter/index');
	}
}