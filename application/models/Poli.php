<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Poli extends CI_Model{
		public function get_all()
		{
			return $this->db->get('tbl_poli');
		}

		public function detail($id_poli)
		{
			return $this->db->get_where('tbl_poli',['id_poli'=>$id_poli]);
		}

		public function insert($data=array())
		{
			return $this->db->insert('tbl_poli', $data);
		}

		public function update($data=array(), $id_poli)
		{
			$this->db->where('id_poli', $id_poli);
			return $this->db->update('tbl_poli', $data);
		}

		public function delete($id_poli)
		{
			$val = array(
				'id_poli' => $id_poli
			);
			return $this->db->delete('tbl_poli', $val);
		}
	}