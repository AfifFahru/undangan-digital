<?php
	function get_user(){
		$CI = get_instance();
		$CI->load->model('Pengguna');
		$username = $CI->session->userdata('username');
		if (!empty($username)) {
			$data = $CI->Pengguna->detail($username)->row_array();
		} else {
			$data = array();
		}
		return @$data;
	}
	function get_pasien(){
		$CI = get_instance();
		$CI->load->model('Pasien');
		$username = $CI->session->userdata('username');
		if (!empty($username)) {
			$data = $CI->Pasien->detail($username)->row_array();
		} else {
			$data = array();
		}
		return @$data;
	}
	function get_antrian($idpoli){
		$CI = get_instance();
		$CI->load->model('Antrian');
		$username = $CI->session->userdata('username');
		$nik_login = $CI->session->userdata('nik_login');
		$antrian['total_antrian'] = @$CI->Antrian->get_total_antrian($idpoli)->row_array()['total_antrian'];
		$antrian['no_antrian'] = @$CI->Antrian->get_no_antrian($idpoli,$nik_login)->row_array()['no_antrian'];
		if (empty($antrian['no_antrian'])) {
			$antrian['no_antrian'] = '-';
		}
		return @$antrian;
	}
	function getHari($idHari){		
		$hari = $idHari;
		if (!empty($idHari)) {
			if ($idHari==1) {
				$hari='Senin';
			} else if ($idHari==2) {
				$hari='Selasa';
			} else if ($idHari==3) {
				$hari='Rabu';
			} else if ($idHari==4) {
				$hari='Kamis';
			} else if ($idHari==5) {
				$hari='Jumat';
			} else if ($idHari==6) {
				$hari='Sabtu';
			} else if ($idHari==7) {
				$hari='Minggu';
			}
		}
		return $hari;
	}
	function getPoli($id_poli){
		$CI = get_instance();
		$CI->load->model('Poli');
		$data = $CI->Poli->detail($id_poli)->row_array();
		$nama_poli='';
		if (!empty($data)) {
			$nama_poli = @$data['nama_poli'];
		}
		return @$nama_poli;
	}